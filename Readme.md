# Docker

This project comprises of 2 docker configurations - a Spark-Kafka-Confluent Schema registry and a Cassandra. This setup can be used as the development environment for a Spark - Cassandra - Kafka based project.

## Spark Kafka
This docker has Spark and Kafka (Confluent.io) configured. Spark runs on a standalone Yarn installation - which may change later.

## Cassandra
This docker has Cassandra installed.
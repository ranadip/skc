#!/bin/bash

function check_ret {
    ret=$1
    if [[ $ret -ne 0 ]]
    then
        echo $2
        exit $ret
    fi
}

which docker
check_ret $? "docker command not working as expected" 

#docker build -t cassandra-base:2.2.4 docker-cassandra/cassandra-base 
#check_ret $? "Cassandra-base docker creation failed" 
#docker build -t cassandra-singlenode:2.2.4 docker-cassandra/cassandra
#check_ret $? "Cassandra docker creation failed" 

docker build -t spark:1.6.1 spark
check_ret $? "Spark Kafka docker creation failed" 

docker build -t confluent:2.11.7 kafka-confluent
check_ret $? "Spark Kafka docker creation failed" 

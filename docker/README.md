# Dockers

## docker-cassandra

This is a clone of https://github.com/spotify/docker-cassandra. 
Cassandra 2.2.4

## spark

Spark 1.6.1

## kafka

Confluent 2.11.7

## docker-compose.yml

Docker compose is used to configure the containers currectly to be able to interact properly.
Use: 
docker-compose up : To create and start the containers
docker-compose stop/start : To stop and start containers
docker-compose down : To stop and remove containers

## setup and stop scripts

Not used any more
